# Stage 1: Build React app
FROM node:latest AS builder
WORKDIR /app
COPY MyFirstReactProject .
RUN npm install
RUN npm run build

# Stage 2: Serve the built app using a lightweight server
FROM nginx:alpine
COPY --from=builder /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
