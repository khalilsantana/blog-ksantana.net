import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import Pills from './components/Pills.jsx'
import Avatar from './components/Avatar.jsx'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    {/* <Header /> */}
    <Avatar />
    <App />
    <Pills/>
  </React.StrictMode>,
)
