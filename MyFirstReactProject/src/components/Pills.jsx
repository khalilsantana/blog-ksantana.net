import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { faGithub, faGitlab, faDiscord, faFontAwesome } from '@fortawesome/free-brands-svg-icons'
import './Pills.css'

library.add(fas, faGithub, faGitlab, faDiscord, faFontAwesome)

const pillItems = [
    { title: 'E-mail', url: "mailto:contato@ksantana.net", icon: "fa-solid fa-inbox" },
    { title: 'Github', url: "https://github.com/KhalilSantana", icon: "fa-brands fa-github" },
    { title: 'Discord', url: "https://discordapp.com/users/khalil.santana", icon: "fa-brands fa-discord" },
    { title: 'Gitlab', url: "https://gitlab.com/KhalilSantana", icon: "fa-brands fa-gitlab" },
];

export default function Pills() {
    const pills = pillItems.map(pill =>
        <div className="child" key={pill.title}>
            <a href={pill.url} target='_blank'>
                <button>
                    <FontAwesomeIcon icon={pill.icon} className="buttonPill" />
                    {pill.title}
                </button></a>
        </div>
    );

    return (
        <div className="parent">{pills}</div>
    )
}