import { Fade } from "react-awesome-reveal";
import Pills from "./Pills";
const Avatar = () => {
    return (
        <div>
            <Fade>
                <img className="avatar" src="/face-200.webp"></img>
            </Fade>
        </div>
    );
}
export default Avatar;